Description

(A clear and concise description of what the bug is.)


Steps to reproduce

(A small piece of code which reproduces the issue with as few as possible external dependencies)

NESSI version

(version *i.e* 0.3.0)


Python version

(version *i.e* 3.5, 3.5+, 3.6.2)


Platform

(OS+version *i.e* Debian 9, Ubuntu 16.10, ...)


Possible fixes

(If you can, link to the line of code that might be responsible for the problem)
